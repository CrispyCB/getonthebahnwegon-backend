FROM golang:1.17

WORKDIR /getonthebahnwegon-backend

COPY helpers ./helpers/
COPY database ./database/
COPY data ./data/
COPY handlers ./handlers/
COPY main ./main/
COPY go.mod ./
COPY go.sum ./

WORKDIR /getonthebahnwegon-backend/main

RUN go get -d -v
RUN go install -v .

RUN go build -o main .

CMD ["./main"]