package data

type Project struct {
	Id            int      `json:"id"`
	Name          string   `json:"name"`
	Description   string   `json:"description"`
	Technologies  []string `json:"technologies"`
	SourceCodeUrl string   `json:"sourceCodeUrl"`
	LiveUrl       string   `json:"liveUrl"`
	ImageUrl      string   `json:"imageUrl"`
	ProjectType   string   `json:"projectType"`
}
