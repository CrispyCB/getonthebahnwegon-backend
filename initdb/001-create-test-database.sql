CREATE TABLE IF NOT EXISTS projects (
    id serial PRIMARY KEY,
    name varchar(256) NOT NULL,
    description text,
    technologies varchar(256)[] NOT NULL,
    source_code_url varchar(256),
    live_url varchar(256) NOT NULL,
    image_url varchar(256) NOT NULL,
    type varchar(256) NOT NULL,
    created_at timestamp DEFAULT now()
);