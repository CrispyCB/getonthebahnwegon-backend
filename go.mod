module getonthebahnwegon-backend

go 1.16

require (
	github.com/lib/pq v1.10.2
	github.com/rs/cors v1.8.2
)
