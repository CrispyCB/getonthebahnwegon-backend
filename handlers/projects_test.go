package handlers

import (
	"encoding/json"
	"getonthebahnwegon-backend/data"
	"getonthebahnwegon-backend/database"
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
)

func TestGetAllProjectsHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/projects", nil)
	if err != nil {
		t.Fatal(err)
	}

	tr := httptest.NewRecorder()
	db, _ := database.SetupDatabase()
	s := Service{
		Database: *db,
		Log:      *log.Default(),
	}
	s.GetAllProjects(tr, req)
	if status := tr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	var res []data.Project
	empty := []data.Project{}
	project1 := &data.Project{
		Id:            0,
		Name:          "The Designs of Dieter Rams",
		Description:   "My version of the tribute site project for FreeCodeCamp, centering around the industrial designer Dieter Rams. Built as a single-page site using HTML, CSS and JavaScript, deployed with GitLab CI and hosted on Amazon S3.",
		Technologies:  []string{"https://getonthebahnwegon.s3.amazonaws.com/project-tech-logos/html5.svg", "https://getonthebahnwegon.s3.amazonaws.com/project-tech-logos/css3.svg", "https://getonthebahnwegon.s3.amazonaws.com/project-tech-logos/javascript.svg"},
		SourceCodeUrl: "https://gitlab.com/CrispyCB/designs-of-dieter-rams",
		LiveUrl:       "https://designsofdieterrams.getonthebahnwegon.com/",
		ImageUrl:      "https://getonthebahnwegon.s3.amazonaws.com/project-thumbnails/dieter_rams_thumbnail.png",
		ProjectType:   "frontend"}
	expected := append(empty, *project1)

	unmarshalErr := json.Unmarshal(tr.Body.Bytes(), &res)
	if unmarshalErr != nil {
		panic(unmarshalErr)
	}
	if len(expected) < len(res) || len(expected) > len(res) {
		t.Errorf("handler returned unexpected body, got %+v want %+v", len(res), len(expected))
	}
	for k, v := range res {
		if v.Name != expected[k].Name {
			t.Errorf("handler returned unexpected body: got %+v want %+v", tr.Body.String(), expected)
		}
	}
}

func TestGetProjectsByTypeHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/projects/type/frontend", nil)
	if err != nil {
		t.Fatal(err)
	}

	tr := httptest.NewRecorder()
	db, _ := database.SetupDatabase()
	s := Service{
		Database: *db,
		Log:      *log.Default(),
	}
	s.GetAllProjects(tr, req)
	if status := tr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	var res []data.Project
	empty := []data.Project{}
	project1 := &data.Project{
		Id:            0,
		Name:          "The Designs of Dieter Rams",
		Description:   "My version of the tribute site project for FreeCodeCamp, centering around the industrial designer Dieter Rams. Built as a single-page site using HTML, CSS and JavaScript, deployed with GitLab CI and hosted on Amazon S3.",
		Technologies:  []string{"https://getonthebahnwegon.s3.amazonaws.com/project-tech-logos/html5.svg", "https://getonthebahnwegon.s3.amazonaws.com/project-tech-logos/css3.svg", "https://getonthebahnwegon.s3.amazonaws.com/project-tech-logos/javascript.svg"},
		SourceCodeUrl: "https://gitlab.com/CrispyCB/designs-of-dieter-rams",
		LiveUrl:       "https://designsofdieterrams.getonthebahnwegon.com/",
		ImageUrl:      "https://getonthebahnwegon.s3.amazonaws.com/project-thumbnails/dieter_rams_thumbnail.png",
		ProjectType:   "frontend"}
	expected := append(empty, *project1)

	unmarshalErr := json.Unmarshal(tr.Body.Bytes(), &res)
	if unmarshalErr != nil {
		panic(unmarshalErr)
	}
	if len(expected) < len(res) || len(expected) > len(res) {
		t.Errorf("handler returned unexpected body, got %+v want %+v", len(res), len(expected))
	}
	for k, v := range res {
		if v.Name != expected[k].Name {
			t.Errorf("handler returned unexpected body: got %+v want %+v", tr.Body.String(), expected)
		}
	}
}

func TestGetSingleProjectHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/projects/0", nil)
	if err != nil {
		t.Fatal(err)
	}

	tr := httptest.NewRecorder()
	db, _ := database.SetupDatabase()
	s := Service{
		Database: *db,
		Log:      *log.Default(),
	}
	s.GetSingleProject(tr, req)
	if status := tr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	var res data.Project
	expected := data.Project{
		Id:            0,
		Name:          "The Designs of Dieter Rams",
		Description:   "My version of the tribute site project for FreeCodeCamp, centering around the industrial designer Dieter Rams. Built as a single-page site using HTML, CSS and JavaScript, deployed with GitLab CI and hosted on Amazon S3.",
		Technologies:  []string{"https://getonthebahnwegon.s3.amazonaws.com/project-tech-logos/html5.svg", "https://getonthebahnwegon.s3.amazonaws.com/project-tech-logos/css3.svg", "https://getonthebahnwegon.s3.amazonaws.com/project-tech-logos/javascript.svg"},
		SourceCodeUrl: "https://gitlab.com/CrispyCB/designs-of-dieter-rams",
		LiveUrl:       "https://designsofdieterrams.getonthebahnwegon.com/",
		ImageUrl:      "https://getonthebahnwegon.s3.amazonaws.com/project-thumbnails/dieter_rams_thumbnail.png",
		ProjectType:   "frontend",
	}

	unmarshalErr := json.Unmarshal(tr.Body.Bytes(), &res)
	if unmarshalErr != nil {
		panic(unmarshalErr)
	}

	if res.Name != expected.Name {
		t.Errorf("handler returned unexpected body: got %+v want %+v", tr.Body.String(), expected)
	}
}

func TestCreateProjectHandler(t *testing.T) {
	form := url.Values{}
	form.Add("name", "Passward")
	form.Add("description", "A password manager built in Golang.")
	form.Add("technologies", "Golang")
	form.Add("technologies", "React")
	form.Add("technologies", "Docker")
	form.Add("source_code_url", "www.gitlab.com/crispycb/passward")
	form.Add("live_url", "www.passward.com")
	form.Add("image_url", "N/A")
	form.Add("type", "full-stack")
	req, err := http.NewRequest("POST", "/api/project", strings.NewReader(form.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	if err != nil {
		t.Fatal(err)
	}

	tr := httptest.NewRecorder()
	db, _ := database.SetupDatabase()
	s := Service{
		Database: *db,
		Log:      *log.Default(),
	}
	s.CreateProject(tr, req)
	if status := tr.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusCreated)
	}
}
