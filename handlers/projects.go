package handlers

import (
	"encoding/json"
	"getonthebahnwegon-backend/data"
	"getonthebahnwegon-backend/database"
	"log"
	"net/http"
	"strings"

	"github.com/lib/pq"
)

type Service struct {
	Database database.DB
	Log      log.Logger
}

func (s *Service) GetAllProjects(w http.ResponseWriter, r *http.Request) {
	s.Log.Println("Getting all projects.")
	sqlSelect := `SELECT id, name, description, technologies, source_code_url, live_url, image_url, type FROM projects;`

	project := data.Project{}
	projects := []data.Project{}

	rows, rowsErr := s.Database.Query(sqlSelect)
	if rowsErr != nil {
		s.Log.Fatalf("Error executing SQL statement: %+v", rowsErr)
	}
	defer rows.Close()
	for rows.Next() {
		scanErr := rows.Scan(&project.Id, &project.Name, &project.Description, pq.Array(&project.Technologies), &project.SourceCodeUrl, &project.LiveUrl, &project.ImageUrl, &project.ProjectType)
		projects = append(projects, project)
		if scanErr != nil {
			s.Log.Fatalf("Error scanning SQL values into struct fields: %+v", scanErr)
		}
	}
	s.Log.Printf("Projects returned: %+v", projects)
	json.NewEncoder(w).Encode(projects)
}

func (s *Service) GetProjectsByType(w http.ResponseWriter, r *http.Request) {
	t := strings.TrimPrefix(r.URL.Path, "/api/projects/type/")
	s.Log.Printf("Getting projects by type: %+v", t)

	sqlSelect := `SELECT id, name, description, technologies, source_code_url, live_url, image_url, type FROM projects WHERE type=$1;`

	project := data.Project{}
	projects := []data.Project{}

	rows, rowsErr := s.Database.Query(sqlSelect, t)
	if rowsErr != nil {
		s.Log.Fatalf("Error executing SQL statement: %+v", rowsErr)
	}
	defer rows.Close()
	for rows.Next() {
		scanErr := rows.Scan(&project.Id, &project.Name, &project.Description, pq.Array(&project.Technologies), &project.SourceCodeUrl, &project.LiveUrl, &project.ImageUrl, &project.ProjectType)
		projects = append(projects, project)
		if scanErr != nil {
			s.Log.Fatalf("Error scanning SQL values into struct fields: %+v", scanErr)
		}
	}
	s.Log.Printf("Returned projects: %+v", projects)
	json.NewEncoder(w).Encode(projects)
}

func (s *Service) GetSingleProject(w http.ResponseWriter, r *http.Request) {
	id := strings.TrimPrefix(r.URL.Path, "/api/projects/")
	sqlSelect := `SELECT id, name, description, technologies, source_code_url, live_url, image_url, type FROM projects WHERE id=$1;`
	s.Log.Printf("Getting project with ID of %+v", id)

	project := &data.Project{}

	scanErr := s.Database.QueryRow(sqlSelect, id).Scan(&project.Id, &project.Name, &project.Description, pq.Array(&project.Technologies), &project.SourceCodeUrl, &project.LiveUrl, &project.ImageUrl, &project.ProjectType)
	if scanErr != nil {
		s.Log.Fatalf("Error scanning SQL values into struct fields: %+v", scanErr)
	}
	s.Log.Printf("Returned project with ID of %+v: %+v", id, project)
	json.NewEncoder(w).Encode(project)
}

func (s *Service) CreateProject(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	s.Log.Printf("Creating project with values: %+v", r.Form)

	sqlInsert := `INSERT INTO projects (name, description, technologies, source_code_url, live_url, image_url, type) VALUES ($1, $2, $3, $4, $5, $6, $7);`

	_, execErr := s.Database.Exec(sqlInsert, r.FormValue("name"), r.FormValue("description"), pq.Array(r.Form["technologies"]), r.FormValue("source_code_url"), r.FormValue("live_url"), r.FormValue("image_url"), r.FormValue("type"))
	if execErr != nil {
		s.Log.Fatalf("Error happened on SQL statement execution. Error: %s", execErr)
	}
	resp := make(map[string]string)
	resp["message"] = "Project created!"
	marshaledResponse, marshalErr := json.Marshal(resp)
	if marshalErr != nil {
		s.Log.Fatalf("Error happened in JSON marshal. Err: %s", marshalErr)
	}
	s.Log.Println("Project created.")
	w.WriteHeader(http.StatusCreated)
	w.Write(marshaledResponse)
}
