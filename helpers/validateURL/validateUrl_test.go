package helpers

import "testing"

func TestValidateURL(t *testing.T) {
	url := "https://gitlab.com/CrispyCB/getonthebahnwegon-backend"
	want := true
	valid, err := ValidateURL(url)

	if valid != want || err != nil {
		// %t for booleans
		// %v for error value
		t.Fatalf(`ValidateUrl("https://gitlab.com/CrispyCB/getonthebahnwegon-backend") = %t, %v want match for %t, nil`, valid, err, want)
	}
}

func TestValidateURLFailure(t *testing.T) {
	url := "https://github.com/CrispyCB/getonthebahnwegon"
	want := false
	valid, err := ValidateURL(url)

	if valid != want || err != nil {
		// %t for booleans
		// %v for error value
		t.Fatalf(`ValidateUrl("https://github.com/CrispyCB/getonthebahnwegon") = %t, %v want match for %t, nil`, valid, err, want)
	}
}
