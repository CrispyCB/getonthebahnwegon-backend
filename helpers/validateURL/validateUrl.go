package helpers

import "regexp"

func ValidateURL(url string) (bool, error) {
	// check if URL string contains substring "gitlab"
	valid, err := regexp.MatchString("gitlab", url)
	// if an error is produced, return false
	// (because we need to return a boolean anyway)
	// along with the error message
	if err != nil {
		return false, err
	}
	// otherwise, return validity boolean and no error
	return valid, nil
}
