package main

import (
	"getonthebahnwegon-backend/database"
	"getonthebahnwegon-backend/handlers"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/rs/cors"
)

func main() {
	// use default logger and print to
	// std out
	l := log.Default()

	// set up serveMux.
	projectsMux := http.NewServeMux()

	db, err := database.SetupDatabase()

	projectService := handlers.Service{
		Database: *db,
		Log:      *l,
	}

	projectsMux.HandleFunc("/api/projects/", projectService.GetSingleProject)
	projectsMux.HandleFunc("/api/projects/type/", projectService.GetProjectsByType)
	projectsMux.HandleFunc("/api/projects", projectService.GetAllProjects)
	projectsMux.HandleFunc("/api/project", projectService.CreateProject)

	// set up HTTP web server using some defaults
	// defined by Nic Jackson
	// only difference is use of default/new logger
	Addr := os.Getenv("ADDR")
	c := cors.Default().Handler(projectsMux)
	s := http.Server{
		Addr:         Addr,
		Handler:      c,
		ErrorLog:     l,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  120 * time.Second,
	}

	l.Printf("Starting server on port %+v", Addr)
	SvErr := s.ListenAndServe()
	// if error, handle
	if SvErr != nil {
		l.Print("Error starting server", "error", err)
		os.Exit(1)
	}
}
